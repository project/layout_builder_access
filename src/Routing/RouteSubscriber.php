<?php

namespace Drupal\layout_builder_access\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for layout_builder.update_block routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $route_names = [
      'layout_builder.update_block',
      'layout_builder.translate_block',
      'layout_builder.translate_inline_block',
      'layout_builder_contextual_link.update_block',
      'layout_builder_contextual_link.translate_block',
      'layout_builder_contextual_link.translate_inline_block',
    ];

    foreach ($route_names as $route_name) {
      $route = $collection->get($route_name);
      if (isset($route)) {
        $route->setRequirement('_layout_builder_update_block_access_check', 'TRUE');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -100];
    return $events;
  }

}
