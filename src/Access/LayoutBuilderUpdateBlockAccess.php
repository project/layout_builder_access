<?php

namespace Drupal\layout_builder_access\Access;

use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\RouteMatch;

/**
 * Checks access.
 */
class LayoutBuilderUpdateBlockAccess implements AccessInterface {

  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, RouteMatch $route_match) {
    $account_roles = $account->getRoles();
    if (in_array('administrator', $account_roles)) {
      return AccessResult::allowed();
    }

    /** @var \Drupal\layout_builder\OverridesSectionStorageInterface $section_storage */
    $section_storage = $route_match->getParameter('section_storage');
    $delta = $route_match->getParameter('delta');
    $uuid = $route_match->getParameter('uuid');
    $component = $section_storage->getSection($delta)->getComponent($uuid);

    $update_access_roles = $component->get('update_access_roles');
    if (is_array($update_access_roles)) {
      if (!empty(array_intersect($update_access_roles, $account_roles))) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::neutral();
  }

}
