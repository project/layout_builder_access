<?php

namespace Drupal\layout_builder_access\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\layout_builder\Form\ConfigureBlockFormBase;
use Drupal\layout_builder\LayoutBuilderHighlightTrait;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\user\Entity\Role;

/**
 * Provides a form to update a block.
 *
 * @internal
 *   Form classes are internal.
 */
class BlockAccessForm extends ConfigureBlockFormBase {

  use LayoutBuilderHighlightTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'layout_builder_block_access';
  }

  /**
   * Builds the block form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage being configured.
   * @param int $delta
   *   The delta of the section.
   * @param string $region
   *   The region of the block.
   * @param string $uuid
   *   The UUID of the block being updated.
   *
   * @return array
   *   The form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL, $delta = NULL, $region = NULL, $uuid = NULL) {
    $component = $section_storage->getSection($delta)->getComponent($uuid);
    $form_state->set('component', $component);
    $form['#attributes']['data-layout-builder-target-highlight-id'] = $this->blockUpdateHighlightId($uuid);
    $form = $this->doBuildForm($form, $form_state, $section_storage, $delta, $component);
    foreach ($form['settings'] as $key => $value) {
        $form['settings'][$key]['#access'] = FALSE;
    }

    $roles = Role::loadMultiple();
    $roles_options = [];
    /** @var  $value */
    foreach ($roles as $key => $value) {
      $roles_options[$key] = $key;
    }
    $form['settings']['update_access_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this
        ->t('Update Access'),
      '#options' => $roles_options,
      '#default_value' => $component->get('update_access_roles'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function submitLabel() {
    return $this->t('Update');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\layout_builder\SectionComponent $component */
    $component = $form_state->get('component');
    $component->set('update_access_roles', $form_state->getValue('settings')['update_access_roles']);
    parent::submitForm($form, $form_state);
  }

}
